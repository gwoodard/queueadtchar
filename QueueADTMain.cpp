/* 

George Woodard
11/13/2014
Programming Assignment
Main Queue ADT

*/

#include <iostream>
#include "QueueADT.h"

using namespace std;

int main()
{
	itemType item;
	char choice;
	QueueADT q;

	//Ask for user input for the Queue and loops until the user has no more input with a Yes or No question
	do{
		cout<<"Enter an item: ";
		cin>>item; //Accepts input
		q.enqueue(item);//Enters item into queue

		//ask for any other items
		cout<<"Do you have another item? (Y/N): ";
		cin>>choice;

	}
	//When the user inputs 'N' for No it will break the do loop, print every input and point to the postition in the list
	while (toupper(choice) != 'N');

	cout<<endl<<"The Queue is: "<< endl << "FRONT <--";
	
	//While the list is empty it will print out each item in the list
	while(!(q.isEmpty()))
	{
		cout<<q.getfront() << "\t<-- ";
		q.dequeue();
	}
	//this designates the rear of the list
	cout<<"REAR" << endl;


	system("pause");


	return 0;
}